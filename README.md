# Security Reports examples

This repository is used to populate data for features like:

- [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html)
- [Dynamic Application Security Testing reports](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html)
- [Dependency Scanning reports](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html)
- [Container Scanning reports](https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html)
- [License management reports](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html)
- [Coverage Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
- [Cluster Image Scanning reports](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/index.html)

## Usage

1. [Fork](https://gitlab.com/gitlab-examples/security/security-reports/-/forks/new) this project
1. Run a new pipeline on the default branch.
1. Profit!


## Development

The [`.gitlab-ci.yml`](https://gitlab.com/gitlab-examples/security/security-reports/-/blob/master/.gitlab-ci.yml) file
contains one job per scanner report.

Most of the jobs consist of downloading an up-to-date scanner report of the relevant type and using it as one of the
[`artifacts:reports`](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreports).

### Dependency scanning

Dependency scanning is not using pre-generated reports because of: https://gitlab.com/gitlab-org/gitlab/-/issues/231309#note_384553418

The `./dependency-scanning-files` directory contains [supported package manager lock files](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers).

### Generic Details

The security report schema supports a [details](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/src/security-report-format.json#L417) field that provides support for displaying [various data elements](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/src/vulnerability-details-format.json#L13) on screen. An example screenshot and accompanying json file is located in `samples/details-example` folder for illustrative purposes.
